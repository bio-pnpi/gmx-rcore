#include "newfit.h"

double F (const DVec &ai, const DVec &biR, const DVec &angl) {
    const double cosA {cos(angl[0])}, cosB {cos(angl[1])}, cosC {cos(angl[2])}, sinA {sin(angl[0])}, sinB {sin(angl[1])}, sinC {sin(angl[2])};
    return  sqrt(   pow(ai[0] + biR[1] * (cosA * sinC - cosC * sinA * sinB) - biR[2] * (sinA * sinC + cosA * cosC * sinB) - cosB * cosC * biR[0], 2) +
                    pow(ai[1] - biR[1] * (cosA * cosC + sinA * sinB * sinC) + biR[2] * (cosC * sinA - cosA * sinB * sinC) - cosB * sinC * biR[0], 2) +
                    pow(ai[2] + sinB * biR[0] - cosA * cosB * biR[2] - cosB * sinA * biR[1], 2)  );
}

template< typename T1, typename T2 >
void checkIfNan(T1 &a, const T2 &b) {
    if (!std::isnan(b)) {
        a += b;
    } else {
        a += 0.;
    }
}

void searchF0xyzabc (long double &F, double &Fx, double &Fy, double &Fz, double &Fa, double &Fb, double &Fc, const RVec &ai, const RVec &biR, const DVec &angl) {
    const double cosA {cos(angl[0])}, cosB {cos(angl[1])}, cosC {cos(angl[2])}, sinA {sin(angl[0])}, sinB {sin(angl[1])}, sinC {sin(angl[2])};
    double t01 {pow(ai[0] + biR[1] * (cosA * sinC - cosC * sinA * sinB) - biR[2] * (sinA * sinC + cosA * cosC * sinB) - cosB * cosC * biR[0], 2)};
    double t02 {pow(ai[1] - biR[1] * (cosA * cosC + sinA * sinB * sinC) + biR[2] * (cosC * sinA - cosA * sinB * sinC) - cosB * sinC * biR[0], 2)};
    double t03 {pow(ai[2] + sinB * biR[0] - cosA * cosB * biR[2] - cosB * sinA * biR[1], 2)};
    double t04 {sqrt(t01 + t02 + t03)};
    double t05 {(ai[0] + biR[1] * (cosA * sinC - cosC * sinA * sinB) - biR[2] * (sinA * sinC + cosA * cosC * sinB) - cosB * cosC * biR[0])};
    double t06 {(ai[2] + sinB * biR[0] - cosA * cosB * biR[2] - cosB * sinA * biR[1])};
    double t07 {(ai[1] - biR[1] * (cosA * cosC + sinA * sinB * sinC) + biR[2] * (cosC * sinA - cosA * sinB * sinC) - cosB * sinC * biR[0])};

    checkIfNan(F, t04);
    checkIfNan(Fx, -(2 * cosB * cosC * t05 - 2 * sinB * t06 + 2 * cosB * sinC * t07) / (2 * t04));
    checkIfNan(Fy, -(2 * (cosA * cosC + sinA * sinB * sinC) * t07 - 2 * (cosA * sinC - cosC * sinA * sinB) * t05 + 2 * cosB * sinA * t06) / (2 * t04));
    checkIfNan(Fz, -(2 * (sinA * sinC + cosA * cosC * sinB) * t05 - 2 * (cosC * sinA - cosA * sinB * sinC) * t07 + 2 * cosA * cosB * t06) / (2 * t04));
    checkIfNan(Fa, -(2 * (cosA * cosB * biR[1] - cosB * sinA * biR[2]) * t06 -
            2 * (biR[1] * (cosC * sinA - cosA * sinB * sinC) + biR[2] * (cosA * cosC + sinA * sinB * sinC)) * t07 +
            2 * (biR[1] * (sinA * sinC + cosA * cosC * sinB) + biR[2] * (cosA * sinC - cosC * sinA * sinB)) * t05) / (2 * t04));
    checkIfNan(Fb, -(2 * (cosA * cosB * sinC * biR[2] - sinB * sinC * biR[0] + cosB * sinA * sinC * biR[1]) * t07 +
            2 * (cosA * cosB * cosC * biR[2] - cosC * sinB * biR[0] + cosB * cosC * sinA * biR[1]) * t05 -
            2 * (cosB * biR[0] + sinA * sinB * biR[1] + cosA * sinB * biR[2]) * t06) / (2 * t04));
    checkIfNan(Fc, (2 * (biR[1] * (cosA * cosC + sinA * sinB * sinC) - biR[2] * (cosC * sinA - cosA * sinB * sinC) + cosB * sinC * biR[0]) * t05 -
            2 * (biR[2] * (sinA * sinC + cosA * cosC * sinB) - biR[1] * (cosA * sinC - cosC * sinA * sinB) + cosB * cosC * biR[0]) * t07) / (2 * t04));
}

void ApplyFit (std::vector< RVec > &b, const DVec &R, const DVec &angl) {
    DVec t(0., 0., 0.);
    const double cosA {cos(angl[0])}, cosB {cos(angl[1])}, cosC {cos(angl[2])}, sinA {sin(angl[0])}, sinB {sin(angl[1])}, sinC {sin(angl[2])};
    for (auto &i : b) {
        t = i.toDVec();
        i[0] = static_cast< float >((t[2] + R[2]) * (sinA * sinC + cosA * cosC * sinB) - (t[1] + R[1]) * (cosA * sinC - cosC * sinA * sinB) + cosB * cosC * (t[0] + R[0]));
        i[1] = static_cast< float >((t[1] + R[1]) * (cosA * cosC + sinA * sinB * sinC) - (t[2] + R[2]) * (cosC * sinA - cosA * sinB * sinC) + cosB * sinC * (t[0] + R[0]));
        i[2] = static_cast< float >(cosA * cosB * (t[2] + R[2]) - sinB * (t[0] + R[0]) + cosB * sinA * (t[1] + R[1]));
    }
}

void CalcMid (const std::vector< RVec > &a, const std::vector< RVec > &b, DVec &midA, DVec &midB, const std::vector< std::pair< size_t, size_t > > &pairs) {
    for (const auto &i : pairs) {
        midA += a[i.first].toDVec();
        midB += b[i.second].toDVec();
    }
    midA[0] /= pairs.size();
    midA[1] /= pairs.size();
    midA[2] /= pairs.size();

    midB[0] /= pairs.size();
    midB[1] /= pairs.size();
    midB[2] /= pairs.size();
}

void MyFitNew (const std::vector< RVec > &a, std::vector< RVec > &b, const std::vector< std::pair< size_t, size_t > > &pairs, long double FtCnst) {
    double fx {0.}, fy {0.}, fz {0.}, fa {0.}, fb {0.}, fc {0.};
    long double f1 {0.}, f2 {0.}, l {1};
    DVec tempA(0., 0., 0.), tempB(0., 0., 0.), tempC(0., 0., 0.), tempD(0., 0., 0.);
    CalcMid(a, b, tempA, tempB, pairs);
    tempA -= tempB;
    for (const auto &i : pairs) {
        f1 += F(a[i.first].toDVec(), b[i.second].toDVec() + tempA, tempC);
    }
    if (FtCnst == 0) {
        FtCnst = 0.000'001; // experimental
    }
    if (f1 == 0) {
        ApplyFit(b, tempA, tempC);
        return;
    } else {
        while (f1 < f2  || f1 - f2 > FtCnst) {
            f1 = 0.; f2 = 0.; fx = 0.; fy = 0.; fz = 0.; fa = 0.; fb = 0.; fc = 0.; l = 1.;
            for (const auto &i : pairs) {
                searchF0xyzabc(f1, fx, fy, fz, fa, fb, fc, a[i.first], b[i.second] + tempA.toRVec(), tempC);
            }
            while (true) {
                f2 = 0;
                tempB[0] = tempA[0] - l * fx;
                tempB[1] = tempA[1] - l * fy;
                tempB[2] = tempA[2] - l * fz;
                tempD[0] = tempC[0] - l * fa;
                tempD[1] = tempC[1] - l * fb;
                tempD[2] = tempC[2] - l * fc;
                for (const auto &i : pairs) {
                    f2 += F(a[i.first].toDVec(), b[i.second].toDVec() + tempB, tempD);
                }
                if (f2 < f1) {
                    tempA = tempB;
                    tempC = tempD;
                    break;
                } else {
                    if (l == DBL_MIN || l == 0.) { //DBL_TRUE_MIN
                        f2 = f1;
                        break;
                    }
                    l *= 0.1;
                }
            }
        }
        ApplyFit(b, tempA, tempC);
    }
}
