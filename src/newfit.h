#ifndef NEWFIT_H
#define NEWFIT_H

#include <gromacs/trajectoryanalysis/topologyinformation.h>
#include <gromacs/math/vectypes.h>
#include <gromacs/math/vec.h>

#include <iostream>
#include <cfloat>
#include <omp.h>
#include <vector>
#include <cstdio>

using gmx::RVec;
using gmx::DVec;
using namespace gmx;

double F (const DVec &ai, const DVec &bi_plusR, const DVec &angl);

template< typename T1, typename T2 > void checkIfNan(T1 &a, const T2 &b);

void searchF0xyzabc (long double &F, double &Fx, double &Fy, double &Fz, double &Fa, double &Fb, double &Fc, const RVec &ai, const RVec &biR, const DVec &angl);

void ApplyFit (std::vector< RVec > &b, const DVec &R, const DVec &angl);

void CalcMid (const std::vector< RVec > &a, const std::vector< RVec > &b, DVec &midA, DVec &midB, const std::vector< std::pair< size_t, size_t > > &pairs);

void MyFitNew (const std::vector< RVec > &a, std::vector< RVec > &b, const std::vector< std::pair< size_t, size_t > > &pairs, long double FtCnst);

#endif // NEWFIT_H
