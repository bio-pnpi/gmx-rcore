/*
 * This file is part of the GROMACS molecular simulation package.
 *
 * Copyright (c) 2011,2012,2013,2014,2015, by the GROMACS development team, led by
 * Mark Abraham, David van der Spoel, Berk Hess, and Erik Lindahl,
 * and including many others, as listed in the AUTHORS file in the
 * top-level source directory and at http://www.gromacs.org.
 *
 * GROMACS is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * GROMACS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GROMACS; if not, see
 * http://www.gnu.org/licenses, or write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.
 *
 * If you want to redistribute modifications to GROMACS, please
 * consider that scientific software is very special. Version
 * control is crucial - bugs must be traceable. We will be happy to
 * consider code for inclusion in the official distribution, but
 * derived work must not be called official GROMACS. Details are found
 * in the README & COPYING files - if they are missing, get the
 * official version at http://www.gromacs.org.
 *
 * To help us fund GROMACS development, we humbly ask that you cite
 * the research papers on the package. Check out http://www.gromacs.org.
 */

#include <fstream>
#include <iostream>
#include <iomanip>

#include <newfit.h>

#include "gromacs/analysisdata/analysisdata.h"
#include "gromacs/analysisdata/modules/average.h"
#include "gromacs/analysisdata/modules/plot.h"
#include "gromacs/options/basicoptions.h"
#include "gromacs/options/filenameoption.h"
#include "gromacs/options/options.h"
#include "gromacs/selection/nbsearch.h"
#include "gromacs/selection/selectionoption.h"
#include "gromacs/trajectory/trajectoryframe.h"
#include "gromacs/trajectoryanalysis/analysismodule.h"
#include "gromacs/trajectoryanalysis/analysissettings.h"
#include "gromacs/trajectoryanalysis/cmdlinerunner.h"

using namespace gmx;


/*! \brief
 * Template class to serve as a basis for user analysis tools.
 */
class RCore : public TrajectoryAnalysisModule
{
    public:

        void initOptions(IOptionsContainer          *options,
                                 TrajectoryAnalysisSettings *settings) override;
        void initAnalysis(const TrajectoryAnalysisSettings &settings,
                                  const TopologyInformation        &top) override;

        void analyzeFrame(int frnr, const t_trxframe &fr, t_pbc *pbc,
                                  TrajectoryAnalysisModuleData *pdata) override;

        void finishAnalysis(int nframes) override;
        void writeOutput() override;

    private:
        //class ModuleData;

        std::string                         outputFName;        // selectable
        Selection                           sel_;
        std::vector< RVec >                 reference;
        std::vector< std::vector < RVec > > trajectory;
        std::vector< int >                  index;
        double                              epsi        {0.25};  // selectable
        double                              prt         {0.66}; // selectable
};

void
RCore::initOptions(IOptionsContainer          *options,
                   TrajectoryAnalysisSettings *settings)
{
    static const char *const desc[] = {
        "Analysis tool for finding molecular core."
    };

    // Add the descriptive text (program help text) to the options
    settings->setHelpText(desc);
    // Add option for selecting a subset of atoms
    options->addOption(SelectionOption("select")
                        .store(&sel_).required()
                        .description("Atoms that are considered as part of the excluded volume"));
    // Add option for output file name
    options->addOption(FileNameOption("on").filetype(OptionFileType::Index).outputFile()
                        .store(&outputFName).defaultBasename("rcore")
                        .description("Index file from the rcore"));
    // Add option for epsi constant
    options->addOption(DoubleOption("eps")
                        .store(&epsi)
                        .description("thermal vibrations' constant"));
    // Add option for particion constant
    options->addOption(DoubleOption("prt")
                        .store(&prt)
                        .description("blob partition"));
    settings->setFlags(TrajectoryAnalysisSettings::efNoUserPBC);
    settings->setFlag(TrajectoryAnalysisSettings::efUseTopX);
    settings->setPBC(true);
}

void
RCore::initAnalysis(const TrajectoryAnalysisSettings &settings,
                    const TopologyInformation        &top)
{
    index.resize(0);
    for (ArrayRef< const int >::iterator ai {sel_.atomIndices().begin()}; ai < sel_.atomIndices().end(); ++ai) {
        index.push_back(*ai);
    }

    reference.resize(0);
    if (top.hasFullTopology()) {
        for (size_t i {0}; i < index.size(); ++i) {
            reference.push_back(top.x().at(index[i]));
        }
    }

    trajectory.resize(0);
}

void
RCore::analyzeFrame(int                          frnr,
                    const t_trxframe             &fr,
                    t_pbc                        *pbc,
                    TrajectoryAnalysisModuleData *pdata)
{
    trajectory.resize(trajectory.size() + 1);
    trajectory.back().resize(0);
    for (size_t i {0}; i < index.size(); ++i)
    {
        trajectory.back().push_back(fr.x[index[i]]);
    }
}

void
RCore::finishAnalysis(int nframes)
{
    std::vector< std::pair< size_t, size_t > > fitPairs;
    fitPairs.resize(0);
    for (size_t i {0}; i < index.size(); ++i) {
        fitPairs.emplace_back(i, i);
    }

    std::vector< std::pair< size_t, double > > noise;

    size_t old = index.size(), nw = 0;
    while (old != nw) {
        noise.resize(0);
        for (size_t i {0}; i < index.size(); ++i) {
            noise.emplace_back(i, 0.);
        }
        for (size_t i {0}; i < trajectory.size(); ++i) {
            MyFitNew(reference, trajectory[i], fitPairs, 0.000'001);
        }
        for (size_t i {0}; i < trajectory.size(); ++i) {
            for (size_t j {0}; j < trajectory[i].size(); ++j) {
                noise[j].second += (reference[j] - trajectory[i][j]).norm();
            }
        }
        for (size_t i {0}; i < noise.size(); ++i) {
            noise[i].second /= trajectory.size();
        }
        std::sort(noise.begin(), noise.end(), [](const std::pair< size_t, double > a, const std::pair< size_t, double > b){ return a.second < b.second;});
        if (size_t temp {static_cast<size_t>(fitPairs.size() * prt)}; noise[temp].second > epsi) {
            fitPairs.resize(0);
            for (size_t i {0}; i <= temp; ++i) {
                fitPairs.emplace_back(noise[i].first, noise[i].first);
            }
            old = nw;
            nw = temp;
        } else {
            old = nw;
            std::ofstream file(outputFName, std::ofstream::app);
            file << "[ rcore ]\n";
            for (size_t i {0}; i < temp; ++i) {
                file << std::setw(7) << noise[i].first;
                if (temp % 7 == 0) {
                    file << "\n";
                }
            }
        }
    }
}

void
RCore::writeOutput()
{

}

/*! \brief
 * The main function for the analysis template.
 */
int
main(int argc, char *argv[])
{
    return gmx::TrajectoryAnalysisCommandLineRunner::runAsMain<RCore>(argc, argv);
}
